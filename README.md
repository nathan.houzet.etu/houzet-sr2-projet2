Agent Flopbox   
Nathan Houzet  || Youva Mokeddes  
2021

# Introduction 

L'objectif du projet est désormais de développer une application cliente pour la plate-forme FlopBox qui permette de synchroniser les données stockées à distance dans un ensemble de serveurs FTP avec le système de fichiers local d'une machine sur laquelle l'application cliente sera exécutée.
# Lancer le programme

Une [démo du programme](./doc/Demo.mp4) est disponible dans le dossier doc.
La démo illustre toutes les fonctionnalités implémentées

Pour lancer le serveur il faut : 
 - se placer à la racine du projet dans le dossier "houzet-sr2-projet2"
 - executer la commande `mvn package` ( si tout est bien configuré, un dossier target 
   avec un executable "flopboxClient-exe-1.0-jar-with-dependencies.jar" est généré )
 - executer le jar avec la commande `java -jar target/flopboxClient-exe-1.0-jar-with-dependencies.jar http://localhost:8080/flopbox/`
 Avec l'adresse de sa plateforme flopbox en paramètre.
   
   Pour lancer la même plateforme flopbox quand dans la démo:
   - Lancer la commande : `java -jar doc/simple-service-1.
     0-SNAPSHOT-jar-with-dependencies.jar`
   
Une copie de l'archive Jar est disponible dans le dossier doc.
L'agent est ainsi lancé et va s'éxécuter.

Les dossiers locaux se trouve dans le répertoire /tmp/{alias}

**Remarque**
Il est important d'avoir l'API flopbox de lancé et au moins un server disponible dont les caractéristiques sont renseignés dans le fichier `.conf` se situant avec les sources.


   
L'agent n'est pas complétement fonctionnel.
Il n'as pas des fonctionnalité d'Upload de fichiers car nous avons eu du mal pour 
réaliser 
les requètes HTTP d'upload. Et il n'opère également que sur fichier racine. La 
récursion n'as pas été traité.

# Architecture

Le programme est principalement composé de 3 classes.



**Une classe RequestServer** : 

Cette classe à été concu dans l'idée de représenter un des multiples serveurs que peux 
contenir l'agent flopbox. Ainsi en terme d'objet, on aura ainsi un object par server 
FTP. Ainsi chaque server fonctionne de manière indépendante avec l'agent.

**Une classe Synchronise** : 

Cette classe s'occupe de synchroniser le serveur avec son dépot local.

**Une classe Main** :

Cette classe lance l'agent et s'occuper de créer un object RequestServer pour quand 
server de l'agent flopbox.

Pour connaitre tout les servers qu'utilise l'API flopbox, le main s'appui sur un 
fichier de configuration ou chaque ligne correspond au informations d'un server.
Le fichier est de ce type : 

<host> <port> <alias> <username> <password>


Diagrammes UML des classes : 

![Classes crées.](./doc/flopbox.png)


**Les exceptions** : 

De plus, 2 exceptions ont été créés pour préciser les différentes problèmes qu'il peut y 
avoir lors de l'éxecution du programme : 

- Une exception pour le téléchargement, si la plateforme ne trouve pas le fichier à 
  télécharger, alors l'exception  "NoContentToDownloadException" est levé.

- Une exception par rapport au upload, si il y à un problème d'upload alors l'exception 
  "ErrorOfUploadException" est levé.

 

# Parcours de code

** Remarque**

```java
public class Synchronise extends TimerTask 
```

La classe synchronize hérite de la classe Timertask afin d'alléger les classe 
intervenantes dans le projet tout en ayant la capacitée de timer les synchronisations




# Tests

Quelques tests ont été réalisé sur les méthodes pouvant être testés facilement

![Tests crées.](./doc/Tests.png)

# Documentation

La documentation est disponible dans le dossier doc via le fichier [*index.html*](./doc/index.html). 
