package flopboxClient;

/***
 * Exception made in case of file to download doesnt exist
 */
public class NoContentToDownloadException extends Exception{
    /**
     * Builder of the class
     */
    public NoContentToDownloadException(){
        super("Error while downloading a file from the flopbox");
    }

}
