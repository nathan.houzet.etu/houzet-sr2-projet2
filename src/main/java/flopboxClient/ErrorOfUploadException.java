package flopboxClient;

/***
 * Exception made in case of file to upload doesnt exist
 */
public class ErrorOfUploadException extends Exception{
    /**
     * Builder of the class
     */
    public ErrorOfUploadException(){
        super("Error while upload a file to the flopbox");
    }

}
