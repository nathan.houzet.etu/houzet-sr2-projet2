package flopboxClient;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * @author Houzet Nathan & Youva Mokkedes
 * This class if made to communicate with one server of the flopbox app
 * So its store all essential element to interact wwith it
 */
public class RequestServer {

    String port;
    String host;
    String FLOPBOXURL;
    String alias;
    String username;
    String password;
    String pathToDir;
    String pathToBinDir;

    /**
     * Builder of this class
     * @param host the host of the server  linked to this object
     * @param port the port of the server linked to this object
     * @param alias linked to this object
     * @param baseURL the base URL  linked to this object (host+alias+port)
     */
    public RequestServer(String host, String port, String alias,String baseURL) {
        this.host = host;
        this.port = port;
        this.alias = alias;
        this.username = "anonymous";
        this.password = "anonymous";
        this.FLOPBOXURL = baseURL;

        //création fichier local et corbeile

        this.pathToDir = "/tmp/"+alias+"/";
        this.pathToBinDir="/tmp/"+alias+"/.del/";
        File theDir = new File(pathToDir);
        File theBinDir = new File(pathToDir);
        if (!theDir.exists()){
            theDir.mkdirs();
        }
        if (!theBinDir.exists()){
            theBinDir.mkdirs();
        }

    }

    /***
     * Setter of the Username and the password for this server
     * @param username the client username for the server
     * @param password  the client password for the server
     */
    public void addAnAccount(String username, String password){
        this.username = username;
        this.password = password;
    }

    /**
     * Used to reset the server information about the login and the password
     */
    public void resetDefaultAccount(){
        this.username = "anonymous";
        this.password = "anonymous";
    }

    /***
     * @param alias the alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /***
     * Method used to add a server to the flopbox
     * @param server The server to add to the flopbox
     * @throws IOException if they are a problem while sending the request
     */
    public void AjouterUnServeur(String server) throws IOException {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost request = new HttpPost(FLOPBOXURL + server);

        // add request headers
        CloseableHttpResponse response = httpClient.execute(request);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            // return it as a String
            String result = EntityUtils.toString(entity);
            //System.out.println(result);


        }
    }

    /***
     * Method that send the HTTP request to add a alias on the server
     * @param alias the alias to give to the server
     * @throws IOException if they are a problem while sending the request
     */
    public void AjouterUnAlias(String alias) throws IOException {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPut request = new HttpPut(FLOPBOXURL + this.host);
        request.addHeader("alias", alias);

        // add request headers
        CloseableHttpResponse response = httpClient.execute(request);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            // return it as a String
            String result = EntityUtils.toString(entity);
            //System.out.println(result);


        }
    }

    /***
     * Method that send the HHTP resquest to gather the server name
     * @throws IOException if they are a problem while sending the request
     */
    public void RecupererLeNomDuServer() throws IOException {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(FLOPBOXURL + this.alias);

        // add request headers
        CloseableHttpResponse response = httpClient.execute(request);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            // return it as a String
            String result = EntityUtils.toString(entity);
        }
    }

    /***
     * sent the request that list the current folder
     * @return a string with all the information about the current folder
     * @throws IOException if they are a problem while sending the request
     */
    public String list() throws IOException {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(FLOPBOXURL + this.alias+"/");
        addLoginHeader(request);
        // add request headers


        CloseableHttpResponse response = httpClient.execute(request);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            // return it as a String
            return EntityUtils.toString(entity);
        }
        return "";
    }

    /***
     * Used to full the request header
     * @param request the request to send
     */
    private void addLoginHeader(HttpGet request) {
        request.addHeader("username", username);
        request.addHeader("password", password);
        request.addHeader("port", port);
    }

    /***
     * Used to full the request header
     * @param request the request to send
     */
    private void addLoginHeader(HttpPost request) {
        request.addHeader("username", username);
        request.addHeader("password", password);
        request.addHeader("port", port);
    }


    /***
     * Method used to send the HTTP request to the Flopbox to download a file
     * @param path the name of the file to download
     * @param dest the name of the download destination
     * @throws IOException If they are a probleme while sending the request
     * @throws NoContentToDownloadException if it doesnt exist a such file
     */
    public void Download(String path, String dest) throws IOException, NoContentToDownloadException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(FLOPBOXURL + this.alias+"/file/"+path);
        addLoginHeader(request);
        // add request headers

        CloseableHttpResponse response = httpClient.execute(request);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            // return it as a String
            String result = EntityUtils.toString(entity);
            PrintWriter writer = new PrintWriter("/tmp/"+dest+path, "UTF-8");
            writer.println(result);
            writer.close();


        }else{
            throw new NoContentToDownloadException() ;
        }
    }

    /***
     * Method used to download a full folder
     * @param prefix the names of the folders
     * @throws IOException if they are a input/output problem while downloading a file
     * @throws NoContentToDownloadException if they are no a such file called to download
     * @throws ParseException If they are a problem while calling the getRemoteDate function
     */
    public void DownloadAll(String prefix) throws IOException, NoContentToDownloadException, ParseException {
        String[] res = this.list().split("\n");
        for (String e :res){
            e = e.trim().replaceAll(" +", " ");
            String[] nameTab =e.split(" ");
            if (e.startsWith("d")){
                //DownloadAll(nameTab[nameTab.length-1]+"/");
            }
            else{
                if (! e.startsWith("l")) {
                    Download(prefix + nameTab[nameTab.length - 1], this.alias + "/" + prefix);
                    File tmp = new File(this.pathToDir+nameTab[nameTab.length - 1]);
                    tmp.setLastModified(getRemoteDate(nameTab).getTime());
                }
            }
        }
    }

    /***
     * Send a HTTP request to the flexbox App to upload a file
     * @param path the path of the file to upload
     * @throws IOException if they are an error
     * @throws ErrorOfUploadException if they are an error while uploading a file
     */
    public void Upload(String path) throws IOException, ErrorOfUploadException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost request = new HttpPost(FLOPBOXURL + this.alias+"/file/"+path);
        addLoginHeader(request);

        File initialFile = new File(this.pathToDir+path);
        InputStream targetStream = new FileInputStream(initialFile);

        HttpEntity entity = new ByteArrayEntity(IOUtils.toByteArray(targetStream));
        request.setEntity(entity);


        // add request headers


        CloseableHttpResponse response = httpClient.execute(request);
        System.out.println("sent");
        entity = response.getEntity();
        if (entity != null) {
            // return it as a String
            String result = EntityUtils.toString(entity);
            System.out.println(result);
        }
        else{ throw new ErrorOfUploadException();}
    }

    /***
     * Function used to get the date of a remote file
     * @param remoteFileSplit The array of information of the remote file
     * @return The date of modification of this file
     * @throws ParseException if they are a problem while parsing the month
     */
    public Date getRemoteDate(String[] remoteFileSplit) throws ParseException {
        Date RemoteDate = new SimpleDateFormat("MMMM").parse(remoteFileSplit[remoteFileSplit.length - 4]);
        RemoteDate.setYear(121); // set 1900 + 121
        RemoteDate.setDate(Integer.parseInt((remoteFileSplit[remoteFileSplit.length - 3])));
        if (remoteFileSplit[remoteFileSplit.length - 2].split(":").length==2){
            RemoteDate.setHours(Integer.parseInt(remoteFileSplit[remoteFileSplit.length - 2].split(":")[0]));
            RemoteDate.setMinutes(Integer.parseInt(remoteFileSplit[remoteFileSplit.length - 2].split(":")[1]));
        }
        else {
            RemoteDate.setYear(Integer.parseInt(remoteFileSplit[remoteFileSplit.length - 2])-1900);
        }
//        Calendar cal = Calendar.getInstance();
//        if (RemoteDate.getMonth()>cal.get(Calendar.MONTH)){
//            RemoteDate.setYear(RemoteDate.getYear()-1);
//        }
        return RemoteDate;
    }
}
