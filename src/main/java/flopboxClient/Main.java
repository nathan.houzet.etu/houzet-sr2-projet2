package flopboxClient;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Timer;

/**
 * @author Nathan Houzet & Youva Mokkedes
 * Main class, create one RequestServer for each server of the flopbox app.
 * Then launch the synchronization for each one
 */
public class Main {
    public static void main(String[] args) {
        try {
            String current = new java.io.File(".").getCanonicalPath();
            BufferedReader br = new BufferedReader(new FileReader(current + "/src/main/java/flopboxClient/.conf"));
            String line;
            ArrayList<RequestServer> servers = new ArrayList<>();
            RequestServer rs;
            // pour chaques lignes dans le fichier de configuartion on associe le server à un object RequestServer
            while ((line = br.readLine()) != null) {

                // on va ajouter les information essentiels sur le server mais il est aussi possible de faire ldes operation de maintenances des servers pour les ajouter sans passer par l API
                String[] lineTab = line.split(" ");
                rs = new RequestServer(lineTab[0], lineTab[1], lineTab[2], args[0]);
                rs.addAnAccount(lineTab[3], lineTab[4]);
                rs.AjouterUnServeur(rs.host);
                rs.AjouterUnAlias(rs.alias);

                // on ititalise le dépot local
                rs.DownloadAll("");
                servers.add(rs);
            }

            // on lance le processus de syncronisation pour chaque server du flopbox
            assert servers != null;
            for (RequestServer ser : servers){

                Timer timer = new Timer();
                timer.schedule(new Synchronise(ser), 0, 5000);
            }

        }catch(Exception e)
    {
        System.out.print(e);
    }
}
}
