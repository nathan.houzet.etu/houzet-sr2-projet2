package flopboxClient;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

/***
 * @author Nathan Houzet & Youva Mokkedes
 * This class will manage all aspect of the synchronisation between the flopbox and the flopbox agent
 */
public class Synchronise extends TimerTask {

    RequestServer rs;

    /***
     * this class need to know the server on witch it is associated to synchronize files with it.
     * @param rs the associated server
     */
    public Synchronise(RequestServer rs){
        this.rs = rs;
    }

    /***
     * this method will detect all the date changes between the files of the local folder and the folder on the server
     * @param prefix a prefix to save the path in recursive cases
     * @throws IOException If the requestServer class have some trouble to communicate with the flopbox API
     * @throws ParseException If they are some parsing errors with calculus of the dates
     * @throws NoContentToDownloadException If they are a problem while downloading some files
     * @throws ErrorOfUploadException If they are an error while upload a file
     */
    public void check(String prefix) throws IOException, ParseException, NoContentToDownloadException, ErrorOfUploadException {
        // recupération fichiers locaux
        File local = new File(rs.pathToDir);
        String[] names = local.list();
        // récupération fichier distant
        String res = rs.list();
        String[] distant = res.split("\n");


        assert names != null;
        List<String> toDel = new LinkedList<>(Arrays.asList(names));
        List<String> toDownload = new LinkedList<>();
        boolean find;

        // on verifie le type de chaque fichier ou dossier distant
        for (String remoteFile :distant){
            find=false;
            if(remoteFile.startsWith("d")){
                // gerer récursion
                //TODO
            }
            else{
                if (! remoteFile.startsWith("l")){
                    // Si c'est un fichier, on va alors cherson son homologue en local en vérifiant les noms'

                    remoteFile = remoteFile.trim().replaceAll(" +", " "); // on enleve les espaces en trop
                    String[] remoteFileSplit = remoteFile.split(" "); //on split selon les espaces

                    for (String localFile :names){

                        if (localFile.equals(remoteFileSplit[remoteFileSplit.length-1])){

                            //On a trouvé sont homologue local, on met a jour nos listes des fichiers a supprimer

                            find = true;
                            toDel.remove(localFile);

                            // on récupères les dates de dernieres modifications des fichiers locaux et distant
                            File LocalFile = new File(this.rs.pathToDir+localFile);
                            Date RemoteDate = rs.getRemoteDate(remoteFileSplit);
                            Date LocalDate = new Date(LocalFile.lastModified());

                            // on compare ces dates pour savoir si il faut uploader, ou telechager le fichier
                            if(RemoteDate.after(LocalDate)){
                                rs.Download(remoteFileSplit[remoteFileSplit.length-1],rs.alias + "/" + prefix);
                            }
                            if(LocalDate.after(RemoteDate)){
                                rs.Upload(localFile);
                            }
                        }
                    }
                }
            }
            //on met a jours non élément non trouvé qui seront a télécharger
            if (!find){
                if(!remoteFile.startsWith("l") && !remoteFile.startsWith("d"))
                toDownload.add(remoteFile.split(" ")[remoteFile.split(" ").length-1]);
            }
        }
        // on supprime les fichiers qui sont en trop sur le depot local
        delFilesInLocal(toDel,local);
        toDownload(toDownload);

    }

    /***
     * Function used to upload a file
     * @param toDownload the file to download
     * @throws IOException If they are a problem while sending the http request
     * @throws NoContentToDownloadException if the file doest exist
     */
    private void toDownload(List<String> toDownload) throws IOException, NoContentToDownloadException {
        for(String e : toDownload){
            this.rs.Download(e,rs.alias + "/");
        }
    }

    /***
     * Method used to deleted files in the local folder and save them in a trash
     * @param toDel The list of files to del from the local folder
     * @param local the path of the local folder
     */
    private void delFilesInLocal(List<String> toDel, File local){
        File tmp;
        for (String e : toDel){
            tmp = new File (local.getAbsolutePath()+"/"+e);
            //System.out.println(local.getAbsolutePath()+"/"+e);
            tmp.renameTo(new File(this.rs.pathToDir+e));
            //System.out.println(this.rs.pathToBinDir+e);
            tmp.delete();
        }
    }


    /***
     * Timer that check the state of the synchronisation every 5 second
     */
    public void run() {
        try {
            this.check("");
        } catch (IOException | ParseException | NoContentToDownloadException | ErrorOfUploadException e) {
            e.printStackTrace();
        }
    }
}
