package flopboxClient;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;

import static org.junit.Assert.*;

/**
 */
public class TestCases
{
    /**
     * Rigorous Test :-)
     */

    RequestServer sr;

    @Before
    public void init() {
        sr = new RequestServer("google.fr", "2121", "perso", "http://localhost:8080/flopbox/");
    }

    @Test
    public void TestResquestServerHost()
    {
        assertTrue(sr.host.equals("google.fr"));
        assertTrue(sr.port.equals("2121"));
        assertTrue(sr.alias.equals("perso"));
        assertTrue(sr.FLOPBOXURL.equals("http://localhost:8080/flopbox/"));

    }

    @Test
    public void TestResquestServerLogin()
    {
        assertTrue(sr.username.equals("anonymous"));
        assertTrue(sr.password.equals("anonymous"));

    }

    @Test
    public void TestResquestServerLoginSet()
    {
        sr.addAnAccount("toto","tata");
        assertTrue(sr.username.equals("toto"));
        assertTrue(sr.password.equals("tata"));
        assertFalse(sr.username.equals("anonymous"));
        assertFalse(sr.password.equals("anonymous"));
    }

    @Test
    public void TestResquestServerLoginUnSet()
    {
        sr.addAnAccount("toto","tata");
        sr.resetDefaultAccount();
        assertTrue(sr.username.equals("anonymous"));
        assertTrue(sr.password.equals("anonymous"));
    }

    @Test
    public void TestResquestServerAlias()
    {
        sr.setAlias("alias");
        assertTrue(sr.alias.equals("alias"));
        assertFalse(sr.alias.equals("perso"));

    }

    @Test
    public void TestResquestServerDirPath() {
        assertTrue(sr.pathToDir.equals("/tmp/"+"perso"+"/"));
    }

    @Test
    public void TestResquestServerBinPath() {
        assertTrue(sr.pathToBinDir.equals("/tmp/"+"perso"+"/.del/"));
    }

    @Test
    public void GetRemoteDate() throws ParseException {
        String[] tmp = {"qe","qp", "qo", "12344", "apr" , "17", "2020", "fichier"};
        Date res = new Date();
        res.setMonth(3);
        res.setYear(120);
        res.setDate(17);
        res.setMinutes(00);
        res.setHours(00);
        res.setSeconds(00);
        assertFalse(res.equals(sr.getRemoteDate(tmp)));
    }


}
